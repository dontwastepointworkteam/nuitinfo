package ronanstudio.fr.preventiondenuit.viewModel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.graphics.Bitmap;

/**
 * Created by A.OUDINOT on 07/12/2017.
 */

public class AcceuilViewModel extends BaseObservable {

    Bitmap img;

    @Bindable
    public Bitmap getImg() {
        return img;
    }

    public void setImg(Bitmap img) {
        this.img = img;
    }
}

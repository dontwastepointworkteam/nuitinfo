package ronanstudio.fr.preventiondenuit.viewModel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import ronanstudio.fr.preventiondenuit.BR;

/**
 * Created by A.OUDINOT on 08/12/2017.
 */

public class NuitRadioViewModel extends BaseObservable {

    private String title;
    private String artist;

    public NuitRadioViewModel() {
        this.title = "";
        this.artist = "";
    }

    @Bindable
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        notifyPropertyChanged(BR.title);
    }

    @Bindable
    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
        notifyPropertyChanged(BR.artist);
    }
}

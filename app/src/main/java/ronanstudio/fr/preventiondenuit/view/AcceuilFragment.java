package ronanstudio.fr.preventiondenuit.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import ronanstudio.fr.preventiondenuit.R;
import ronanstudio.fr.preventiondenuit.databinding.FragmentAcceuilBinding;

/**
 * Created by A.OUDINOT on 07/12/2017.
 */

public class AcceuilFragment extends Fragment implements OnMapReadyCallback {

    private FragmentAcceuilBinding binding;
    private SupportMapFragment mapFragment;

    private GoogleMap mMap;
    private CarouselView carouselView;

    int[] sampleImages = {R.drawable.prev1, R.drawable.prev2, R.drawable.prev3,
            R.drawable.prev4, R.drawable.prev5, R.drawable.prev6, R.drawable.prev7};

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_acceuil, container, false);

        mapFragment = SupportMapFragment.newInstance();
        mapFragment.getMapAsync(this);

        final FragmentManager manager = getFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();

        ft.replace(R.id.map_container,mapFragment);
        ft.commit();

        carouselView = binding.carouselView;

        carouselView.setPageCount(sampleImages.length);

        carouselView.setImageListener(imageListener);

        return binding.getRoot();
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(sampleImages[position]);
        }
    };

    public static AcceuilFragment newInstance() {

        Bundle args = new Bundle();

        AcceuilFragment fragment = new AcceuilFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }
}

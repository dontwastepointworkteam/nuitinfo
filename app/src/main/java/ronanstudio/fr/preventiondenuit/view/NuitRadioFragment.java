package ronanstudio.fr.preventiondenuit.view;

import android.databinding.DataBindingUtil;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import ronanstudio.fr.preventiondenuit.R;
import ronanstudio.fr.preventiondenuit.databinding.FragmentRadioNuitBinding;
import ronanstudio.fr.preventiondenuit.viewModel.NuitRadioViewModel;

public class NuitRadioFragment extends Fragment {

    private FragmentRadioNuitBinding binding;
    private MediaPlayer mediaPlayer;
    private boolean play = false;
    private String URL_radio = "http://www.radioking.com/play/radio-nuit-info";

    NuitRadioViewModel viewModel;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_radio_nuit, container, false);
        binding.setView(this);
        binding.setViewModel(viewModel = new NuitRadioViewModel());


        mediaPlayer = MediaPlayer.create(getContext(), Uri.parse(URL_radio));
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);


//        mediaPlayer.start();

        return binding.getRoot();
    }

    public void onClickPlayPause(View v){
        ImageView imageView = (ImageView) v;
        if (!play) {
            imageView.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.pause_button));
            mediaPlayer.start();
            binding.title.setVisibility(View.VISIBLE);
            binding.titleContent.setVisibility(View.VISIBLE);
            binding.artist.setVisibility(View.VISIBLE);
            binding.artistContent.setVisibility(View.VISIBLE);

            new AsyncTask<Void,Void,Void>(){

                @Override
                protected Void doInBackground(Void... params) {
                    getInfo();
                    return null;
                }
            }.execute();
            play = true;
        } else {
            imageView.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.paly_button));
            mediaPlayer.pause();
            binding.title.setVisibility(View.INVISIBLE);
            binding.titleContent.setVisibility(View.INVISIBLE);
            binding.artist.setVisibility(View.INVISIBLE);
            binding.artistContent.setVisibility(View.INVISIBLE);
            play = false;
        }

    }

    public static NuitRadioFragment newInstance() {

        Bundle args = new Bundle();

        NuitRadioFragment fragment = new NuitRadioFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void getInfo(){
        URL url;
        HttpURLConnection connection;
        JSONObject retour = null;

        try {

            url = new URL("https://www.radioking.com/widgets/currenttrack.php?radio=116593&format=json");

            connection = (HttpURLConnection) url.openConnection();
            BufferedInputStream input = new BufferedInputStream(connection.getInputStream());

            retour = new JSONObject(readAllLines(input));

            viewModel.setArtist(retour.getString("artist"));
            viewModel.setTitle(retour.getString("title"));
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * Transform an {@link InputStream} to {@link String}
     * @throws IOException If an I/O error occurs
     */
    private String readAllLines (InputStream input) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
        StringBuilder builder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }
        return builder.toString();
    }
}

package ronanstudio.fr.preventiondenuit.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ronanstudio.fr.preventiondenuit.R;
import ronanstudio.fr.preventiondenuit.databinding.FragmentRadioTabBinding;
import ronanstudio.fr.preventiondenuit.model.SampleFragmentPagerAdapter;


public class TabRadioFragment extends Fragment {
    FragmentRadioTabBinding binding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_radio_tab, container, false);

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        ViewPager viewPager = binding.viewpager;
        viewPager.setAdapter(new SampleFragmentPagerAdapter(getFragmentManager(), getContext()));

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = binding.tableLayout;
        tabLayout.setupWithViewPager(viewPager);

        return binding.getRoot();
    }

    public static TabRadioFragment newInstance() {

        Bundle args = new Bundle();
        
        TabRadioFragment fragment = new TabRadioFragment();
        fragment.setArguments(args);
        return fragment;
    }
}

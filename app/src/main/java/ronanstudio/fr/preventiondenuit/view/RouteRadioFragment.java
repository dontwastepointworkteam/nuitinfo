package ronanstudio.fr.preventiondenuit.view;

import android.databinding.DataBindingUtil;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.database.DatabaseUtilsCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import ronanstudio.fr.preventiondenuit.R;
import ronanstudio.fr.preventiondenuit.databinding.FragmentRadioNuitBinding;
import ronanstudio.fr.preventiondenuit.databinding.FragmentRadioRouteBinding;

/**
 * Created by A.OUDINOT on 08/12/2017.
 */

public class RouteRadioFragment extends Fragment {
    private FragmentRadioNuitBinding binding;
    private MediaPlayer mediaPlayer;
    private boolean play = false;
    private String URL_radio = "http://mediaming.streamakaci.com/sanef107_7_EST.mp3";

    public static RouteRadioFragment newInstance() {
        
        Bundle args = new Bundle();
        
        RouteRadioFragment fragment = new RouteRadioFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void onClickPlayPause(View v){
        ImageView imageView = (ImageView) v;
        if (!play) {
            imageView.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.pause_button));
            mediaPlayer.start();
            play = true;
        } else {
            imageView.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.paly_button));
            mediaPlayer.pause();
            play = false;
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentRadioRouteBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_radio_route, container, false);
        binding.setView(this);

        mediaPlayer = MediaPlayer.create(getContext(), Uri.parse(URL_radio));
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        return binding.getRoot();
    }
}

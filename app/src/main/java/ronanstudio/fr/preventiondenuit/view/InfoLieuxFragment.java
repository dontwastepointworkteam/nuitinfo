package ronanstudio.fr.preventiondenuit.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ronanstudio.fr.preventiondenuit.R;
import ronanstudio.fr.preventiondenuit.databinding.FragmentInfoLieuxBinding;

/**
 * Created by Geoffrey on 08/12/2017.
 */

public class InfoLieuxFragment extends Fragment {

    private FragmentInfoLieuxBinding binding;

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_info_lieux, container, false);

        return binding.getRoot();
    }

    public static InfoLieuxFragment newInstance() {

        Bundle args = new Bundle();

        InfoLieuxFragment fragment = new InfoLieuxFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
